/**
 *Copyright 2013 by dragon.
 *
 *File name: Log.java
 *Author:      dragon
 *Email:       fufulove2012@gmail.com
 *Blog:        http://blog.csdn.net/xidomlove
 *Version:     1.0.0
 *Date:        2013-10-7 下午4:28:20
 *Description: 
 */
package com.dragon.log;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 简单日志类
 * 
 * @author dragon8
 * 
 */
public class Logger {

	private boolean enable = true;

	private int logLevel = DEBUG;

	private PrintStream out = null;

	private static Logger defaultLogger = null;

	/**
	 * @return the defaultLogger
	 */
	public static Logger getDefaultLogger() {
		return defaultLogger;
	}

	/**
	 * @param defaultLogger
	 *            the defaultLogger to set
	 */
	public static void setDefaultLogger(Logger defaultLogger) {
		Logger.defaultLogger = defaultLogger;
	}

	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	/**
	 * @param dateFormat
	 *            the dateFormat to set
	 */
	void setDateFormat(SimpleDateFormat dateFormat) {
		this.dateFormat = dateFormat;
	}

	/**
	 * @return the logLevel
	 */
	int getLogLevel() {
		return logLevel;
	}

	/**
	 * @param logLevel
	 *            the logLevel to set
	 */
	void setLogLevel(int logLevel) {
		this.logLevel = logLevel;
	}

	public static final int FATAL = 0;
	public static final int ERROR = 1;
	public static final int WARN = 2;
	public static final int INFO = 3;
	public static final int DEBUG = 4;

	/**
	 * @param file
	 *            文件名
	 * @param append
	 *            是否以追加的形式
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 */
	public Logger(String file, boolean append)
			throws UnsupportedEncodingException, FileNotFoundException {
		// TODO Auto-generated constructor stub
		out = new PrintStream(new BufferedOutputStream(new FileOutputStream(
				file, append)), true, "utf-8");
	}

	/**
	 * @param outputStream
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 */
	public Logger(OutputStream outputStream)
			throws UnsupportedEncodingException, FileNotFoundException {
		// TODO Auto-generated constructor stub
		if (outputStream != null) {
			out = new PrintStream(new BufferedOutputStream(outputStream), true,
					"utf-8");
		}
	}

	public void enableLog() {
		enable = true;
	}

	public void disableLog() {
		enable = false;
	}

	public void fatal(String message) {
		if (!enable || logLevel < FATAL) {
			return;
		}
		out.printf("FATAL");
		writeMessage(message);
	}

	public void error(String message) {
		if (!enable || logLevel < ERROR) {
			return;
		}
		out.printf("ERROR");
		writeMessage(message);
	}

	public void warn(String message) {
		if (!enable || logLevel < WARN) {
			return;
		}
		out.printf("WARN ");
		writeMessage(message);
	}

	public void info(String message) {
		if (!enable || logLevel < INFO) {
			return;
		}
		out.printf("INFO ");
		writeMessage(message);
	}

	/**
	 * 打印调试信息
	 * 
	 * @param message
	 */
	public void debug(String message) {
		if (!enable || logLevel < DEBUG) {
			return;
		}
		out.printf("DEBUG");
		writeMessage(message);
	}

	private void writeMessage(String message) {

		StackTraceElement element = Thread.currentThread().getStackTrace()[3];
		synchronized (out) {
			out.printf(" %s--%s----%s %s.%s:%d\r\n",
					dateFormat.format(new Date()), message,
					element.getFileName(), element.getClassName(),
					element.getMethodName(), element.getLineNumber());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#finalize()
	 */
	@Override
	protected void finalize() throws Throwable {
		// TODO Auto-generated method stub
		super.finalize();
		if (out != null) {
			out.close();
			out = null;
		}
	}

	/**
	 * 关闭日志文件
	 */
	public void close() {
		if (out != null) {
			out.close();
			out = null;
		}
	}
}
